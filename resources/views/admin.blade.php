@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <div class="left-menu">
                <ul>
                    <li><a href="{{route('admin.dashboard')}}">Dashboard</a></li>
                    <li><a href="{{route('header.index')}}">Header</a></li>
                    <li><a href="{{route('category.index')}}">Categories</a></li>
                    <li><a href="{{route('suggestion.index')}}">Suggestions</a></li>
                    <li><a href="{{route('physic.index')}}">Physics</a></li>
                    <li><a href="{{route('ict.index')}}">ict</a></li>
                    <li><a href="{{route('sheet.index')}}">class sheet</a></li>
                    <li><a href="{{route('notice.index')}}">notice</a></li>
                    <li><a href="{{route('about.index')}}">about</a></li>
                    <li><a href="{{route('contact.index')}}">contact</a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-10">

            @yield('main-content')

        </div>
    </div>
</div>
@endsection

@extends('admin')
@section('main-content')
    {!! Form::open(['rout'=>'admin.suggestion.store']) !!}
    <!-- Title Form Input -->
    <div class="form-group">
        {{Form::label('title','Title:')}}
        {{Form::text('title',null,['class'=>'form-control'])}}
    </div>
    <!-- Description Form Input -->
    <div class="form-group">
        {{Form::label('description','Description:')}}
        {{Form::text('description',null,['class'=>'form-control'])}}
    </div>
    <!-- Image Form Input -->
    <div class="form-group">
        {{Form::label('image','Image:')}}
        {{Form::file('image',null,['class'=>'form-control'])}}
    </div>
    <!-- Publication_status Form Input -->
    <div class="form-group">
        <select name="publication_status" class="form-control">
            <option value="1">Publish</option>
            <option value="0">Un Publish</option>
        </select>
    </div>
    {!! Form::close() !!}
@endsection
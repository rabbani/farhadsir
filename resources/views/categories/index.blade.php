@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"> CATEGORY </i></div>
        <div class="panel-body">
            <a href="{{route('category.create')}}" class="text-success"><b>ADD NEW+</b></a>
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>image</th>
                    <th>title</th>
                    <th>status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td><img src="{{asset('images/'.$category->image)}}" alt="" width="50" height="50"></td>
                        <td>{{$category->title}}</td>
                        <td>
                            @if($category->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success"> publish </span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger"> un publish </span>
                            @endif

                        </td>
                        <td>
                            {!! HTML::decode(Html::linkRoute('category.show','<i class="fa fa-eye">view</i>', [$category->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! HTML::decode(Html::linkRoute('category.edit','<i class="fa fa-pencil-square-o">edit</i>', [$category->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['category.destroy',$category->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
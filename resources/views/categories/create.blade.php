@extends('resources.views.admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"> CREATE CATEGORY </i></div>
        <div class="panel-body">
        {!! Form::open(['route'=>['category.store'],'files'=>true]) !!}
        <!-- Title Form Input -->
            <div class="form-group">
                {{Form::label('name','Name:')}}
                {{Form::text('name',null,['class'=>'form-control'])}}
            </div>

            <!-- Image Form Input -->
            <div class="form-group">
                {{Form::label('image','Image:')}}
                {{Form::file('image',null,['class'=>'form-control'])}}
            </div>
            <!-- Publication_status Form Input -->
            <div class="form-group">
                {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}
            </div>

            {{Form::submit('save',['class'=>'btn btn-success btn-sm'])}}
            <a href="{{route('category.index')}}" class="btn btn-danger btn-sm">Cancel</a>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
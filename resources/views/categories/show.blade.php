@extends('resources.views.admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book">VIEW HEADER</i></div>
        <div class="panel-body">
            <h3>{{$header->name}}</h3>
            <img src="{{asset('images/'.$header->image)}}" alt="" width="300" height="300" style="float: left">

        </div>
        <div class="panel-footer">
            <a href="{{route('category.index')}}" class="fa fa-arrow-left btn btn-info btn-sm" style="float: left;">back</a>
            {!! HTML::decode(Html::linkRoute('header.edit','<i class="fa fa-pencil-square-o btn btn-warning">edit</i>', [$header->id],['style'=>'margin:0 5px; float:left'])) !!}
            {!! Form::open(['route'=>['header.destroy',$header->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
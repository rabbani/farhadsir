@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"> ABOUTS </i></div>
        <div class="panel-body">
            <a href="{{route('about.create')}}" class="text-success"><b>ADD NEW+</b></a>
            <table class="table table-bordered table-responsive">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Details</th>
                    <th>image</th>
                    <th>status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($abouts as $about)
                    <tr>
                        <td>{{$about->title}}</td>
                        <td class="col-sm-6">{{$about->description}}</td>
                        <td><img src="{{asset('images/'.$about->image)}}" alt="" width="50" height="50"></td>
                        <td>
                            @if($about->publication_status==1)
                                <span class="fa fa-thumbs-o-up text-success">publish</span>
                            @else
                                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
                            @endif

                        </td>
                        <td>
                            {!! HTML::decode(Html::linkRoute('about.show','<i class="fa fa-eye">view</i>', [$about->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! HTML::decode(Html::linkRoute('about.edit','<i class="fa fa-pencil-square-o">edit</i>', [$about->id],['style'=>'margin:5px; float:left'])) !!}
                            {!! Form::open(['route'=>['about.destroy',$about->id],'method'=>'DELETE']) !!}
                            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection
<div class="content-root">
    <div class="main-header">

        @foreach($header as $v_header)
        <div class="header-content-img">
            <img src="{{asset('images/'.$v_header['image'])}}" alt="" class="img-responsive">
        </div>
        <div class="header-content">
            <p>{{$v_header['title']}}</p>
        </div>
            @endforeach
    </div><!-- main-header -->
</div>

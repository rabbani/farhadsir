<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Golam Rabbani</a>
        </div><!-- navbar-header -->
        <div class="collapse navbar-collapse" id="collapse">
            <ul class="nav navbar-nav">
                <li><a href="" class="top-button">Home</a></li>
                <li><a href="#sheet" class="top-button">Sheet</a></li>
                <li><a href="#suggestions" class="top-button">Suggestions</a></li>
                <li><a href="#physics1" class="top-button">Physics 1st</a></li>
                <li><a href="#physics2" class="top-button">Physics 2nd</a></li>
                <li><a href="#ict" class="top-button">ICT</a></li>
                <li><a href="#notice" class="top-button">Notice</a></li>
                <li><a href="#about" class="top-button">About</a></li>
                <li><a href="#about" class="top-button">Contact</a></li>
            </ul>
        </div><!--collapse navbar-collapse #collapse -->
    </div><!-- container -->
</nav>

<div class="container-fluid">
        <div class="row">
            <div class="category-header">
                <div class="category-header-image">
                    <img src="{{asset('images/'.$category['image'])}}" alt="">
                </div><!-- category-image -->
                <div class="category-header-title">
                    <p>{{strtoupper($category['name'])}}</p>
                </div><!-- category-title -->
                <div class="category-header-details">
                    {{--<p>This is physic category</p>--}}
                </div><!-- category-details -->
            </div><!-- category-list -->
        </div>
    </div><!-- container -->
    
    <div class="container">
        <div class="row">
            <div class="category-list">
                <div class="category-image">
                    <img src="{{asset('images/1501083574.gif')}}" alt="">
                </div><!-- category-image -->
                <div class="category-title">
                    <p>physics</p>
                </div><!-- category-title -->
                <div class="category-details">
                    <p>This is physic category</p>
                </div><!-- category-details -->
            </div><!-- category-list -->
        </div>
    </div><!-- container -->

<div class="container">
        <div class="row">
            <div class="category-list">
                <div class="category-image">
                    <img src="{{asset('images/1501083574.gif')}}" alt="">
                </div><!-- category-image -->
                <div class="category-title">
                    <p>physics</p>
                </div><!-- category-title -->
                <div class="category-details">
                    <p>This is physic category</p>
                </div><!-- category-details -->
            </div><!-- category-list -->
        </div>
    </div><!-- container -->
    
    <div class="container">
        <div class="row">
            <div class="category-list">
                <div class="category-image">
                    <img src="{{asset('images/1501083574.gif')}}" alt="">
                </div><!-- category-image -->
                <div class="category-title">
                    <p>physics</p>
                </div><!-- category-title -->
                <div class="category-details">
                    <p>This is physic category</p>
                </div><!-- category-details -->
            </div><!-- category-list -->
        </div>
    </div><!-- container -->
    
    <footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p>Copyright &copy; golam rabbani</p>
            </div><!-- col-sm-6 -->
            <div class="col-sm-6">
                <ul class="nav navbar-nav">
                    <li><a href="">Home</a></li>
                    <li><a href="">Test1</a></li>
                    <li><a href="">Test2</a></li>
                    <li><a href="">Test3</a></li>
                    <li><a href="">Test4</a></li>
                    <li><a href="">About</a></li>
                    <li><a href="">Contact</a></li>
                </ul>
            </div><!-- col-sm-6 -->
        </div><!-- row -->
    </div><!-- container -->
</footer>

<script src="http://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('js/custom.js')}}"></script>
</body>
</html>

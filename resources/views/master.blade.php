<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Golam Rabbani</a>
        </div><!-- navbar-header -->
        <div class="collapse navbar-collapse" id="collapse">
            <ul class="nav navbar-nav">
                <li><a href="" class="top-button">HOME</a></li>
                @foreach($categories as $category)
                <li><a href="{{asset('page/'.$category->id)}}" class="top-button">{{strtoupper($category->name)}}</a></li>
                @endforeach
                {{----}}
                {{--<li><a href="" class="top-button">Home</a></li>--}}
                {{--<li><a href="#sheet" class="top-button">Sheet</a></li>--}}
                {{--<li><a href="#suggestions" class="top-button">Suggestions</a></li>--}}
                {{--<li><a href="#physics1" class="top-button">Physics 1st</a></li>--}}
                {{--<li><a href="#physics2" class="top-button">Physics 2nd</a></li>--}}
                {{--<li><a href="#ict" class="top-button">ICT</a></li>--}}
                {{--<li><a href="#notice" class="top-button">Notice</a></li>--}}
                {{--<li><a href="#about" class="top-button">About</a></li>--}}
                {{--<li><a href="#about" class="top-button">Contact</a></li>--}}
            </ul>
        </div><!--collapse navbar-collapse #collapse -->
    </div><!-- container -->
</nav>

@include('pages.header')

    <div class="container" id="sheet">
    <div class="row">
        <div class="col-sm-7">
            <div class="section">
                <div class="section-title">
                    <h3>{{$sheet['title']}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$sheet['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                   <p>{{strlen($sheet['description'])>250?substr($sheet['description'],0,250).'...':$sheet['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->

<div class="container" id="suggestions">
    <div class="row">
        <div class="col-sm-7 col-sm-offset-5">
            <div class="section">
                <div class="section-title">
                    <h3>{{$suggestions->title}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$suggestions['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                  <p>{{strlen($suggestions['description'])>250?substr($suggestions['description'],0,250).'...':$suggestions['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->


<div class="container" id="physics1">
    <div class="row">
        <div class="col-sm-7">
            <div class="section">
                <div class="section-title">
                    <h3>{{$physics1['title']}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$physics1['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                    <p>{{strlen($physics1['description'])>250?substr($physics1['description'],0,250).'...':$physics1['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->


<div class="container" id="physics2">
    <div class="row">
        <div class="col-sm-7 col-sm-offset-5">
            <div class="section">
                <div class="section-title">
                    <h3>{{$physics2['title']}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$physics2['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                   <p>{{strlen($physics2['description'])>250?substr($physics2['description'],0,250).'...':$physics2['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->


<div class="container" id="ict">
    <div class="row">
        <div class="col-sm-7">
            <div class="section">
                <div class="section-title">
                    <h3>{{$ict['title']}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$ict['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                  <p>{{strlen($ict['description'])>250?substr($ict['description'],0,250).'...':$ict['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->

<div class="container" id="notice">
    <div class="row">
        <div class="col-sm-7 col-sm-offset-5">
            <div class="section">
                <div class="section-title">
                    <h3>{{$notice['title']}}</h3>
                </div>

                <div class="section-img">
                    <img src="{{asset('images/'.$notice['image'])}}" class="img-responsive float-left" alt="">
                </div>


                <div class="section-content">
                    <p>{{strlen($notice['description']>250)?substr($notice['description'],0,250).'...':$notice['description']}}</p>
                    <div class="more">
                        <a href="" class="text-center">More...</a>
                    </div>
                </div><!-- section-content -->
            </div><!-- section -->
        </div><!-- col-sm-6 -->
    </div><!-- row -->
</div><!-- container -->

<div class="container" id="about">
    <div class="row">
        <div class="address">
            <div class="about">
                <h3 class="text-center"><span>About Us</span></h3>
                <p>{{$about['description']}}</p>
            </div><!-- col-sm-6 -->
            <div class="contact">
                <h3 class="text-center"><span>Contact Us</span></h3>
                <p>{{strlen($contact['description'])>250?substr($contact['description'],0,200).'..':$contact['description']}}</p>
                <div class="more">
                    <a href="" class="text-center">Contact</a>
                </div>
            </div><!-- col-sm-6 -->
        </div><!-- address -->
    </div><!-- row -->
</div><!-- container -->

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <p>Copyright &copy; golam rabbani</p>
            </div><!-- col-sm-6 -->
            <div class="col-sm-6">
                <ul class="nav navbar-nav">
                    <li><a href="">Home</a></li>
                    <li><a href="">Test1</a></li>
                    <li><a href="">Test2</a></li>
                    <li><a href="">Test3</a></li>
                    <li><a href="">Test4</a></li>
                    <li><a href="">About</a></li>
                    <li><a href="">Contact</a></li>
                </ul>
            </div><!-- col-sm-6 -->
        </div><!-- row -->
    </div><!-- container -->
</footer>

<script src="http://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('js/custom.js')}}"></script>
</body>
</html>

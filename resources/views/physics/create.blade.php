@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"> CREATE PHYSICS TOPIC </i></div>
        <div class="panel-body">
        {!! Form::open(['route'=>['physic.store'],'files'=>true]) !!}
        <!-- Title Form Input -->
            <div class="form-group">
                {{Form::label('title','Title:')}}
                {{Form::text('title',null,['class'=>'form-control'])}}
            </div>
            <!-- Description Form Input -->
            <div class="form-group">
                {{Form::label('description','Description:')}}
                {{Form::text('description',null,['class'=>'form-control'])}}
            </div>
            <!-- Image Form Input -->
            <div class="form-group">
                {{Form::label('image','Image:')}}
                {{Form::file('image',null,['class'=>'form-control'])}}
            </div>
            <!-- Video Form Input -->
            <div class="form-group">
                {{Form::label('video','Video:')}}
                {{Form::text('video',null,['class'=>'form-control'])}}
            </div>
            <!--select part-->
            <div class="form-group">
               {{ Form::select('paper', ['1' => 'First Paper', '2' => 'Second Paper'], null, ['class'=>'form-control','placeholder' => 'select paper.'])}}
            </div>
            <!-- Chapter Form Input -->
            <div class="form-group">
                {{Form::label('chapter','Chapter:')}}
                {{Form::text('chapter',null,['class'=>'form-control'])}}
            </div>
            <!-- Publication_status Form Input -->
            <div class="form-group">
                {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}
            </div>
            {{Form::submit('save',['class'=>'btn btn-success btn-sm'])}}
            <a href="{{route('physic.index')}}" class="btn btn-danger btn-sm">Cancel</a>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
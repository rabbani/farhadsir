@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book"> VIEW PHYSICS </i></div>
        <div class="panel-body">
    <h3>{{$ict->title}}</h3>
    <p>{{$ict->description}}</p>
    <img src="{{asset('images/'.$physic->image)}}" alt="" width="300" height="300" style="float: left">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$physic->video}}" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="panel-footer">
            <a href="{{route('physic.index')}}" class="fa fa-arrow-left btn btn-info btn-sm" style="float: left;">back</a>
            {!! HTML::decode(Html::linkRoute('physic.edit','<i class="fa fa-pencil-square-o btn btn-warning">edit</i>', [$ict->id],['style'=>'margin:0 5px; float:left'])) !!}
            {!! Form::open(['route'=>['physic.destroy',$physic->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@extends('admin')
@section('main-content')
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-book"> EDIT PHYSICS</i></div>
    <div class="panel-body">
        {!! Form::model($physic,['route'=>['physic.update',$physic->id], 'method'=>'PUT', 'files'=>true]) !!}
        <!-- Title Form Input -->
        <div class="form-group">
            {{Form::label('title','Title:')}}
            {{Form::text('title',null,['class'=>'form-control'])}}
        </div>
        <!-- Description Form Input -->
        <div class="form-group">
            {{Form::label('description','Description:')}}
            {{Form::text('description',null,['class'=>'form-control'])}}
        </div>


        <!--old image-->
        <div class="form-group">
            <h4>old images</h4>
            <img src="{{asset('images/'.$physic->image)}}" alt="" height="200" width="300">
        </div>
        <!-- Image Form Input -->
        <div class="form-group">
            {{Form::label('image','Image:')}}
            {{Form::file('image',null,['class'=>'form-control'])}}
        </div>
        {{--old video link--}}
        <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$physic->video}}" frameborder="0" allowfullscreen></iframe>
        <!-- Video Form Input -->
        <div class="form-group">
            {{Form::label('paper','Paper')}}
            {{Form::text('paper',null,['class'=>'form-control'])}}
        </div>
     <div class="form-group">
        {{Form::label('chapter','Chapter')}}
            {{Form::text('chapter',null,['class'=>'form-control'])}}
    </div>
        <div class="form-group">
            {{Form::label('video','Video:')}}
            {{Form::text('video',null,['class'=>'form-control'])}}
        </div>

        <!-- Publication_status Form Input -->
        <div class="form-group">
            {{ Form::select('publication_status', ['1' => 'Publish', '0' => 'Un Publish'], null, ['placeholder'=>'select status', 'class' => 'form-control'])}}

        </div>
        {{Form::submit('update',['class'=>'btn btn-success btn-sm'])}}
        {!! Form::close() !!}
        <a href="{{route('ict.index')}}" class="fa fa-arrow-left btn btn-info btn-sm"> back</a>
        {!! Form::open(['route'=>['physic.destroy',$physic->id],'method'=>'DELETE']) !!}
        {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
        {!! Form::close() !!}

    </div>
</div>
@endsection
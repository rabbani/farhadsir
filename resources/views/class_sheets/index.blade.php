@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book">CLASS SHEETS</i></div>
        <div class="panel-body">
    <a href="{{route('sheet.create')}}" class="text-success"><b>ADD NEW+</b></a>
<table class="table table-bordered table-responsive">
    <thead>
        <tr>
            <th>Title</th>
            <th>Details</th>
            <th>image</th>
            <th>status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($sheets as $sheet)
    <tr>
        <td>{{$sheet->title}}</td>
        <td class="col-sm-6">{{$sheet->description}}</td>
        <td><img src="{{asset('images/'.$sheet->image)}}" alt="" width="50" height="50"></td>
        <td>
            @if($sheet->publication_status==1)
                <span class="fa fa-thumbs-o-up text-success">publish</span>
            @else
                <span class="fa fa-thumbs-o-down text-danger">un publish</span>
            @endif

        </td>
        <td>
            {!! HTML::decode(Html::linkRoute('sheet.show','<i class="fa fa-eye">view</i>', [$sheet->id],['style'=>'margin:5px; float:left'])) !!}
            {!! HTML::decode(Html::linkRoute('sheet.edit','<i class="fa fa-pencil-square-o">edit</i>', [$sheet->id],['style'=>'margin:5px; float:left'])) !!}
            {!! Form::open(['route'=>['sheet.destroy',$sheet->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o"></i>', ['type' => 'submit','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}
        </td>
    </tr>
        @endforeach
    </tbody>
</table>

    </div>
    </div>
@endsection
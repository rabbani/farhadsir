@extends('admin')
@section('main-content')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-book">VIEW SHEET</i></div>
        <div class="panel-body">
    <h3>{{$sheet->title}}</h3>
    <p>{{$sheet->description}}</p>
    <img src="{{asset('images/'.$sheet->image)}}" alt="" width="300" height="300" style="float: left">

        </div>
        <div class="panel-footer">
            <a href="{{route('sheet.index')}}" class="fa fa-arrow-left btn btn-info btn-sm" style="float: left;">back</a>
            {!! HTML::decode(Html::linkRoute('sheet.edit','<i class="fa fa-pencil-square-o btn btn-warning">edit</i>', [$sheet->id],['style'=>'margin:0 5px; float:left'])) !!}
            {!! Form::open(['route'=>['sheet.destroy',$sheet->id],'method'=>'DELETE']) !!}
            {{  Form::button( '<i class="fa fa-trash-o">delete</i>', ['type' => 'submit','class'=>'btn btn-danger btn-sm','style'=>'margin:0;','onclick'=>'return confirm("Are You Sure You Want To Delete This! ")'])}}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
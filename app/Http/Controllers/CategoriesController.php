<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Storage;
class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function index()
    {
        $categories= Category::orderBy('id','desc')->get();
        return view('categories.index')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'image'=>'sometimes',
            'publication_status'=>'required'
        ]);



        $Category= new Category();
        $Category->name= $request->name;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $Category->image=$file_name;
        }
        $Category->publication_status=$request->publication_status;

        $Category->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category= Category::findOrFail($id);
        return view('categories.show')->withCategory($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category= Category::find($id);
        return view('categories.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Category = Category::findOrFail($id);
        $Category->name=$request->name;
        $Category->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$Category->image;
            $Category->image=$file_name;
            Storage::delete($old_file);
        }
        $Category->publication_status=$request->publication_status;
        $Category->update();


        Session::flash('message','Data update Successfully');
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Category= Category::findOrFail($id);
        Storage::delete($Category->image);
        $Category->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('category.index');
    }
}

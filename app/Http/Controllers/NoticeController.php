<?php

namespace App\Http\Controllers;

use App\Notice;
use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Storage;
class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function index()
    {
        $notices= Notice::orderBy('id','desc')->get();
        return view('notices.index')->withNotices($notices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'sometimes'
        ]);



        $notice= new Notice();
        $notice->title= $request->title;
        $notice->description= $request->description;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $notice->image=$file_name;
        }
        $notice->publication_status=$request->publication_status;

        $notice->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('notices.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notice= Notice::findOrFail($id);
        return view('notices.show')->withNotice($notice);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notice= Notice::find($id);
        return view('notices.edit')->withNotice($notice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice = Notice::findOrFail($id);
        $notice->title=$request->title;
        $notice->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$notice->image;
            $notice->image=$file_name;
            Storage::delete($old_file);
        }
        $notice->publication_status=$request->publication_status;
        $notice->update();


        Session::flash('message','Data update Successfully');
        return redirect()->route('notice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notice= Notice::findOrFail($id);
        Storage::delete($notice->image);
        $notice->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('notice.index');
    }
}

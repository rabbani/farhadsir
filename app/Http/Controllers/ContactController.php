<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Notice;
use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Storage;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
//    public function __construct()
//    {
//        $this->middleware('guest:admin');
//    }

    public function index()
    {
        $contacts= Contact::orderBy('id','desc')->get();
        return view('contacts.index')->withContacts($contacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'sometimes',
            'description'=>'sometimes',
            'image'=>'sometimes',
            'publication_status'=>'required'
        ]);



        $contact= new Contact();
        $contact->title= $request->title;
        $contact->description= $request->description;
        $contact->publication_status=$request->publication_status;
        $contact->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('contact.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact= Contact::findOrFail($id);
        return view('contacts.show')->withContact($contact);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact= Contact::find($id);
        return view('contacts.edit')->withContact($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->title=$request->title;
        $contact->description=$request->description;
        $contact->publication_status=$request->publication_status;
        $contact->update();


        Session::flash('message','Data update Successfully');
        return redirect()->route('contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact= Contact::findOrFail($id);
        Storage::delete($contact->image);
        $contact->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('contact.index');
    }
}

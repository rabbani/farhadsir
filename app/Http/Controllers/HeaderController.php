<?php

namespace App\Http\Controllers;

use App\Header;
use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Storage;
class HeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function index()
    {
        $headers= Header::orderBy('id','desc')->get();
        return view('headers.index')->withHeaders($headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('headers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image'=>'required',
            'title'=>'required',
            'publication_status'=>'required'
        ]);



        $header= new Header();
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $header->image=$file_name;
        }
        $header->title= $request->title;
        $header->publication_status=$request->publication_status;

        $header->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('header.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $header= Header::findOrFail($id);
        return view('headers.show')->withHeader($header);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $header= Header::find($id);
        return view('headers.edit')->withHeader($header);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $header = Header::findOrFail($id);
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$header->image;
            $header->image=$file_name;
            Storage::delete($old_file);
        }
        $header->title=$request->title;
        $header->publication_status=$request->publication_status;
        $header->update();


        Session::flash('message','Data update Successfully');
        return redirect()->route('Header.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $header= Header::findOrFail($id);
        Storage::delete($header->image);
        $header->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('header.index');
    }
}

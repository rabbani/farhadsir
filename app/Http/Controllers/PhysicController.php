<?php

namespace App\Http\Controllers;


use App\Physic;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PhysicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $physics= Physic::orderBy('id','desc')->get();
        return view('physics.index')->withPhysics($physics);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('physics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'sometimes',
            'video'=>'sometimes',
            'paper'=>'required',
            'chapter'=>'required',
            'publication_status'=>'required'

        ]);

        $physic= new Physic();
        $physic->title= $request->title;
        $physic->description= $request->description;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $physic->image=$file_name;
        }
        $physic->video=substr($request->video,32);
        $physic->paper=$request->paper;
        $physic->chapter=$request->chapter;
        $physic->publication_status=$request->publication_status;

        $physic->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('physic.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $physic= Physic::findOrFail($id);
        return view('physics.show')->withphysic($physic);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $physic= Physic::find($id);
        return view('physics.edit')->withphysic($physic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $physic = Physic::findOrFail($id);
        $physic->title=$request->title;
        $physic->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$physic->image;
            $physic->image=$file_name;
            Storage::delete($old_file);
        }
        $physic->video=substr($request->video,32);
        $physic->chapter=$request->chapter;
        $physic->publication_status=$request->publication_status;
        $physic->update();

        Session::flash('message','Data update Successfully');
        return redirect()->route('physic.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $physic= physic::findOrFail($id);
        Storage::delete($physic->image);
        $physic->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('physic.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Suggestion;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suggestions= Suggestion::orderBy('id','desc')->get();
        return view('suggestions.index')->withSuggestions($suggestions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suggestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'title'=>'required',
            'description'=>'required',
            'image'=>'sometimes'
        ]);



       $suggestion= new Suggestion();
       $suggestion->title= $request->title;
       $suggestion->description= $request->description;

       if($request->hasFile('image')){
           $image=$request->file('image');
           $file_name=time().'.'.$image->getClientOriginalExtension();
           $location=public_path('images/'.$file_name);
           Image::make($image)->save($location);
           $suggestion->image=$file_name;
       }
        $suggestion->publication_status=$request->publication_status;

        $suggestion->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('suggestion.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $suggestion= Suggestion::findOrFail($id);
        return view('suggestions.show')->withSuggestion($suggestion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suggestion= Suggestion::find($id);
        return view('suggestions.edit')->withSuggestion($suggestion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $suggestion = Suggestion::findOrFail($id);
        $suggestion->title=$request->title;
        $suggestion->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$suggestion->image;
            $suggestion->image=$file_name;
            Storage::delete($old_file);
        }
        $suggestion->publication_status=$request->publication_status;
        $suggestion->update();

        Session::flash('message','Data update Successfully');
        return redirect()->route('suggestion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suggestion= Suggestion::findOrFail($id);
        Storage::delete($suggestion->image);
        $suggestion->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('suggestion.index');
    }
}

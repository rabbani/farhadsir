<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Ict;
use App\Suggestion;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class IctController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icts= Ict::orderBy('id','desc')->get();
        return view('icts.index')->withIcts($icts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('icts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'title'=>'required',
            'description'=>'required',
            'image'=>'sometimes'
        ]);



       $ict= new Ict();
       $ict->title= $request->title;
       $ict->description= $request->description;

       if($request->hasFile('image')){
           $image=$request->file('image');
           $file_name=time().'.'.$image->getClientOriginalExtension();
           $location=public_path('images/'.$file_name);
           Image::make($image)->save($location);
           $ict->image=$file_name;
       }
        $ict->video=substr($request->video,32);
        $ict->chapter=$request->chapter;
        $ict->publication_status=$request->publication_status;

        $ict->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('ict.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ict= Ict::findOrFail($id);
        return view('icts.show')->withIct($ict);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ict= Ict::find($id);
        return view('icts.edit')->withIct($ict);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ict = Ict::findOrFail($id);
        $ict->title=$request->title;
        $ict->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$ict->image;
            $ict->image=$file_name;
            Storage::delete($old_file);
        }
        $ict->video=substr($request->video,32);
        $ict->chapter=$request->chapter;
        $ict->publication_status=$request->publication_status;
        $ict->update();

        Session::flash('message','Data update Successfully');
        return redirect()->route('ict.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ict= Ict::findOrFail($id);
        Storage::delete($ict->image);
        $ict->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('ict.index');
    }
}

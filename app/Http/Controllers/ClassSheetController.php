<?php

namespace App\Http\Controllers;

use App\Admin;
use App\ClassSheet;
use Illuminate\Support\Facades\Storage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClassSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sheets= ClassSheet::orderBy('id','desc')->get();
        return view('class_sheets.index')->withSheets($sheets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('class_sheets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'image'=>'sometimes'
        ]);



        $sheet= new ClassSheet();
        $sheet->title= $request->title;
        $sheet->description= $request->description;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $sheet->image=$file_name;
        }
        $sheet->publication_status=$request->publication_status;

        $sheet->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('sheet.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sheet= ClassSheet::findOrFail($id);
        return view('class_sheets.show')->withSheet($sheet);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sheet= ClassSheet::find($id);
        return view('class_sheets.edit')->withSheet($sheet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sheet = ClassSheet::findOrFail($id);
        $sheet->title=$request->title;
        $sheet->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$sheet->image;
            $sheet->image=$file_name;
            Storage::delete($old_file);
        }
        $sheet->publication_status=$request->publication_status;
        $sheet->update();

        Session::flash('message','Data update Successfully');
        return redirect()->route('sheet.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sheet= ClassSheet::findOrFail($id);
        Storage::delete($sheet->image);
        $sheet->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('sheet.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\ClassSheet;
use App\Suggestion;
use App\Physic;
use App\Ict;
use App\Notice;
use App\About;
use App\Contact;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories= Category::all();
      $sheet=  ClassSheet::OrderBy('id','desc')->take(1)->first();
      $suggestions= Suggestion::OrderBy('id','desc')->take(1)->first();
       $physics1= Physic::orderBy('id','desc')->where('paper',1)->take(1)->first();
        $physics2= Physic::orderBy('id','desc')->where('paper',2)->take(1)->first();
        $ict=  Ict::orderBy('id','desc')->take(1)->first();
         $notice=  Notice::orderBy('id','desc')->take(1)->first();
          $about= About::orderBy('id','desc')->take(1)->first();
          $contact= Contact::orderBy('id','desc')->take(1)->first();
      
      return view('master')
          ->withCategories($categories)
          ->withSheet($sheet)
              ->withSuggestions($suggestions)
              ->withPhysics1($physics1)
              ->withPhysics2($physics2)
                ->withIct($ict)
               ->withNotice($notice)
               ->withAbout($about)
               ->withContact($contact)
              ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\About;

use Illuminate\Http\Request;
use Image;
use Session;
use Illuminate\Support\Facades\Storage;
class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
   

    public function index()
    {

        $abouts= About::orderBy('id','desc')->get();
        return view('abouts.index')->withAbouts($abouts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('abouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'sometimes',
            'description'=>'sometimes',
            'image'=>'sometimes',
            'publication_status'=>'required'
        ]);



        $about= new About();
        $about->title= $request->title;
        $about->description= $request->description;

        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().'.'.$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $about->image=$file_name;
        }
        $about->publication_status=$request->publication_status;

        $about->save();

        Session::flash('message','Data insert Successfully');
        return redirect()->route('about.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $about= About::findOrFail($id);
        return view('abouts.show')->withAbout($about);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about= About::find($id);
        return view('abouts.edit')->withAbout($about);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::findOrFail($id);
        $about->title=$request->title;
        $about->description=$request->description;
        if($request->hasFile('image')){
            $image=$request->file('image');
            $file_name=time().".".$image->getClientOriginalExtension();
            $location=public_path('images/'.$file_name);
            Image::make($image)->save($location);
            $old_file=$about->image;
            $about->image=$file_name;
            Storage::delete($old_file);
        }
        $about->publication_status=$request->publication_status;
        $about->update();


        Session::flash('message','Data update Successfully');
        return redirect()->route('about.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about= About::findOrFail($id);
        Storage::delete($about->image);
        $about->delete();
        Session::flash('message','data deleted successfully');
        return redirect()->route('about.index');
    }
}

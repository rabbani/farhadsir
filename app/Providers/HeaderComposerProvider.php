<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Header;

class HeaderComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('pages.header', function ($view) {
            $view->with('header', Header::all());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}

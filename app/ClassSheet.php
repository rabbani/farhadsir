<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSheet extends Model
{
    protected $table='class_sheets';
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('category');
//    return view('master');
//});

//index page 
Route::get('/', 'WelcomeController@index');

Auth::routes();
Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');


Route::get('/home', 'HomeController@index')->name('home');


//admin panel
Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    //password reset route
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

    //header
    Route::resource('header', 'HeaderController');
    //category
    Route::resource('category', 'CategoriesController');

    //suggestion
    Route::resource('suggestion', 'SuggestionController');
    //physics
    Route::resource('physic', 'PhysicController');
    //notice
    Route::resource('notice', 'NoticeController');
    //class sheets
    Route::resource('sheet', 'ClassSheetController');
    //about
    Route::resource('about', 'AboutController');
    //contact
    Route::resource('contact', 'ContactController');
    //ict
    Route::resource('ict', 'IctController');


});
//manage single category page and post
Route::get('single/{id}', 'SingleController@show');
Route::resource('page', 'PageController', ['only' => 'show']);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
